'use strict';

/**
 * Dependencies
 * @ignore
 */
const path = require("path");
const express = require("express");
const morgan = require("morgan");

/**
 * Express
 * @ignore
 */
const { PORT: port = 3000 } = process.env;
const app_folder = 'build';
const app = express();

app.use(morgan('tiny'));

/**
 * Serving static files
 * @ignore
 */
app.get('*.*', express.static(app_folder, { maxAge: '1y' }));

/**
 * Serving application paths
 * @ignore
 */
app.get('*', (req, res) => {
  res.status(200).sendFile('/', { root: app_folder });
});

/**
 * Launch app
 * @ignore
 */
app.listen(port, () => console.log(`Example app listening on port ${port}!`));
