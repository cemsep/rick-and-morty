import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import './App.css';
import CharacterList from './CharacterList/CharacterList';
import CharacterProfile from './CharacterProfile/CharacterProfile';
import NotFound from './NotFound/NotFound';

class App extends React.Component {

  render() {
    return (
      <Router>
        <div className="App">
          <header className="App-header"></header>
          <Switch>
            <Redirect exact from="/" to="/characters" />
            <Route exact path="/characters" component={CharacterList} />
            <Route exact path="/profile/:id" component={CharacterProfile} />
            <Route exact path="/not-found" component={NotFound} />
            <Redirect from="*" to="/not-found" />
          </Switch>
        </div>
      </Router>
    );
  }
}

export default App;
