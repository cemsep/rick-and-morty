import React from 'react';
import { withRouter } from 'react-router-dom'
import './CharacterListItem.css';

class CharacterListItem extends React.Component {

  constructor(props) {
    super(props);

    this.state = {};
  }

  /**
   * Redirecting the user to profile page.
   * @ignore
   */
  viewProfile = () => {
    this.props.history.push(`/profile/${this.props.character.id}`);
  }

  render() {
    return (
      <div className="Character-list-item">
        <div className="Character-list-item-header">
          <img className="Character-list-item-image" src={this.props.character.image} alt="profile"/>
          <div className="Character-list-item-name">{this.props.character.name}</div>
        </div>
        <ul className="Character-list-item-stats">
          <li>
            <b>Status</b>
            <span>{this.props.character.status}</span>
          </li>
          <li>
            <b>Species</b>
            <span>{this.props.character.species}</span>
          </li>
          <li>
            <b>Gender</b>
            <span>{this.props.character.gender}</span>
          </li>
          <li>
            <b>Origin</b>
            <span>{this.props.character.origin.name}</span>
          </li>
          <li>
            <b>Last location</b>
            <span>{this.props.character.location.name}</span>
          </li>
        </ul>
        <button className="Character-list-item-button" onClick={this.viewProfile}>View Profile</button>
      </div>
    );
  }

}

export default withRouter(CharacterListItem)