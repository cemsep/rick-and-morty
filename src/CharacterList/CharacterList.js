import React from 'react';
import './CharacterList.css';
import CharacterListItem from '../CharacterListItem/CharacterListItem';

class CharacterList extends React.Component {

  constructor(props) {
    super(props);

    this.state = { loading: true, characters: [], updatedCharacters: [], next: '', search: '' };
  }

  /**
   * Using async start, fetching the first set of characters from the API.
   * Making two instances of the characters result to further usage (for the searchbar).
   * Storing the API link for the next set of characters.
   * @ignore
   */
  componentDidMount() {
    this.asyncLoad().then(data => {
      this.setState({ loading: false, 
        characters: data.results,
        updatedCharacters: data.results,
        next: data.info.next });
    })
  }

  /**
   * Fetching the characters from the API.
   * @ignore
   */
  async asyncLoad() {
    return new Promise((resolve, reject) => {
      fetch('https://rickandmortyapi.com/api/character/') // 'http://localhost:8080/api/character'
      .then(response => response.json())
      .then(data => resolve(data));
    });
  }

  /**
   * Fetching the next set of characters.
   * @ignore
   */
  async asyncLoadMore() {
    // Use this if the API is on the localhost. 
    /* const url = this.state.next.split('/');
    const id = url[url.length - 1]; */

    fetch(this.state.next) // `http://localhost:8080/api/character/${id}`
    .then(response => response.json())
    .then(data => {
      this.setState({ characters: this.state.characters.concat(data.results),
        updatedCharacters: this.state.updatedCharacters.concat(data.results),
        next: data.info.next });
    });
  }

  /**
   * Searching a character. Filternig the characters after a search text.
   * Setting the updatedCharacters state to filtered array.
   * Setting the search state to search text value. If the search state contains a text
   * or not null, the load more button will not be shown.
   * @ignore 
   */
  search(e) {
    var updatedCharacters = this.state.characters;
    updatedCharacters = updatedCharacters.filter(character => {
      return character.name.toLowerCase().search(e.target.value.toLowerCase()) !== -1
    });
    this.setState({ updatedCharacters: updatedCharacters, search: e.target.value });
  }

  render() {
    const { loading } = this.state;

    if(loading) {
      return (
        <div className="Character-list">Loading Rick and Morty characters...</div>
      );
    }

    const items = [];

    for (const [index, value] of this.state.updatedCharacters.entries()) {
      items.push(<CharacterListItem key={index} character={value}></CharacterListItem>)
    }

    return (
      <div className="container">
        <div className="Character-list-search">
          <input type="search" placeholder="Enter a character name..." onChange={this.search.bind(this)}></input>
        </div>
        <div className="Character-list">
          {items}
        </div>
        {(this.state.next && !this.state.search) ? 
        <button className="Character-list-button" onClick={this.asyncLoadMore.bind(this)}>Load More</button>
        : <div></div>}
      </div>
    );
  }

}

export default CharacterList