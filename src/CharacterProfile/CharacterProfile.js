import React from 'react';
import { withRouter } from 'react-router-dom';
import './CharacterProfile.css';

class CharacterProfile extends React.Component {

  constructor(props) {
    super(props);

    this.state = { loading: true, character: {}, location: {} };
  }

  /**
   * Using the async load, fetching the current character by id and his/her location.
   * Updating the states.
   * @ignore
   */
  componentDidMount() {
    this.asyncLoad().then(data => {
      this.setState({ character: data });
      this.asyncLoadLocation(this.state.character)
      .then(location => {
        this.setState({ loading: false, location: location });
      });
    });
  }

  /**
   * Fetching a single character from the API.
   * @ignore
   */
  async asyncLoad() {
    // Getting the characters id from the url.
    const path = this.props.location.pathname.split('/');
    const id = path[path.length - 1];

    return new Promise((resolve, reject) => {
      fetch(`https://rickandmortyapi.com/api/character/${id}`) // `http://localhost:8080/api/character/${id}`
      .then(response => response.json())
      .then(data => resolve(data));
    });
  }

  /**
   * Fetching the characters location from the API. If the location url is null, the location will be unknown.
   * @ignore
   */
  async asyncLoadLocation(character) {
    // Use this if the API is on localhost.
    /* const url = character.location.url.split('/');
    const id = url[url.length - 1]; */

    if(character.location.url === "") {
      return new Promise((resolve, reject) => {
        resolve({name: "unknown", type: "unknown", dimension: "unknown"});
      })
    } else {
      return new Promise((resolve, reject) => {
        fetch(character.location.url) // `http://localhost:8080/api/location/${id}`
        .then(response => response.json())
        .then(data => resolve(data));
      });
    }
  }

  /**
   * Redirecting the user back to home page.
   * @ignore
   */
  back() {
    this.props.history.push('/characters');
  }

  render() {
    const { loading } = this.state;

    if(loading) {
      return <div>Loading character...</div>
    }

    return (
      <div className="container">
        <div className="Character-profile">
          <button className="Character-profile-button" onClick={this.back.bind(this)}>
            <i className="arrow left"></i> Back</button>
          <div className="Profile-header">
            <img src={this.state.character.image} alt="profile" />
            <div className="Profile-header-title">
              <h4>{this.state.character.name}</h4>
              <span>Status: {this.state.character.status}</span>
            </div>
          </div>
          <div className="Profile-info">
            <h4>Profile</h4>
            <ul>
              <li>
                Gender: 
                <span>{this.state.character.gender}</span>
              </li>
              <li>
                Species: 
                <span>{this.state.character.species}</span>
              </li>
              <li>
                Origin: 
                <span>{this.state.character.origin.name}</span>
              </li>
            </ul>
            <h4>Location</h4>
            <ul>
              <li>
                Current Location: 
                <span>{this.state.location.name}</span>
              </li>
              <li>
                Type: 
                <span>{this.state.location.type}</span>
              </li>
              <li>
                Dimension: 
                <span>{this.state.location.dimension}</span>
              </li>
            </ul>
          </div>
        </div>
      </div>
    );
  }

}

export default withRouter(CharacterProfile)