import React from 'react';
import { withRouter } from 'react-router-dom';
import './NotFound.css';
import image from '../Assets/404.png';

class NotFound extends React.Component {

  constructor(props) {
    super(props);

    this.state = {}
  }

  /**
   * Redirecting the user back to home page.
   * @ignore
   */
  back() {
    this.props.history.push('/characters');
  }

  render() {
    return (
      <div className="container">
        <div className="Not-found">
          <button className="Not-found-button" onClick={this.back.bind(this)}>Back Home</button>
          <img src={image} alt="not found" />
        </div>
      </div>
    );
  }

}

export default withRouter(NotFound)